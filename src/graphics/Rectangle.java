package graphics;

//
// A package corresponds to a directory on your system.
// Packages are used to organise your project.
//
// A class contains attributen in which you can save data.
// The current values determine the state of the object.
// Methods are blocks of code that can be executed on objects.
//


// Class declaration:
//      public      : the class can be used anywhere.
//      class       : this is a class.
//      Rectangle   : class Name, must be the same as filename, Rectangle.java.

public class Rectangle {
	// Atrribute  (AKA fields, instance variables, member variables).
	// declaration
	//      private : only accessible from within this class.
	//      int     : type
	//      x       : naam
	// If you do not initialise attributes they receive a null (or 0) value by default

	private int x;
	private int y;
	private int height;
	private int width;

	//
	// Constructor declaration :
	//      called to make a new object of class Rectangle aan te maken.
	//      public : this method can be called from anywhere.
	//
	// You do not use the keyword void because the constructor returns Rectangle.
	//   You can view this as a method withour a name and return type Rectangle.
	//   or as a method without a return type with the same name as the class
	//
	//
	// The constructor is called (in this case from the Main class) to make new objects of the class Rectangle.
	//

	// Constructor without parameters.
	// If you do not supply a constructor, the compiler adds this constructor by default.

	public Rectangle() {
	}

	// Constructor, with parameters.
	public Rectangle(int height, int width) {
		// 'this' refers to the object the method was called upon.
		this.x = 15;            // The instance variabele x is initialised to 15.
		this.y = 150;
		// Left hand side = the attribute 'height' of the object.
		// Right hand side = the height parameter of the method.
		this.height = height;
		this.width = width;
	}

	// Copy constructor : a constructor that makes a new copy another object of this class.
	// The attributes of the other object are used to initialise this new object.
	//
	public Rectangle(Rectangle rectangle) {
		this.x = rectangle.getX();
		this.y = rectangle.getY();
		this.height = rectangle.getHeight();
		// (!) You can always make a copy even if the attributes are private,
		// because the other object is of the same class
		this.width = rectangle.width;
	}

	// Getters en setters (ALT+INSERT): provide these (public) methods if you want to give access to (private) variables from outside this class
	//  Getter is an accessor that supplies the attribute value. It has no parameters and returns a value of the type of the attribute
	//  Setter is a mutator that changes the attribute value. It has the value as a parameter and returns void.
	//
	// Code conventions  : getVariabeleName, setVariabeleName.
	//  remark that xxxVariableName is capitalised in the method, because it is a new word in the middle of the name

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	// Method declaration
	//      public                  : can be called on an object of this class from anywhere.
	//      void                    : return type, nothing in this case.
	//      methodNoArguments       : method name.
	//      ()                      : parameter list. Mandatory even if empty
	//      {}                      : block with code to be executed when method is called.
	//

	public void methodNoArguments() {
		System.out.println("In methodNoArguments of Rectangle.");
	}

	// Method
	//      public                  : is een method die overal gebruikt kan worden.
	//      void                    : return type, in dit geval niets (vertaling van void = leeg).
	//      methodWithArgument      : naam van de method.
	//      (                       : begin van de argumentenlijst.
	//      int newX                : argumentenlijst van variabelen, in dit geval 1 int met naam newX.
	//      )                       : einde van de argumentenlijst.
	//      {}                      : block die de statements bevat die worden uitgevoerd in de method.
	//                              : in dat block kan de parameter gebruikt worden.
	//
	// Wordt aangeroepen in de Main class, op het object rectangle.

	public void methodWithArgument(int newX) {
		System.out.println("In methodWithArgument van Rectangle.");

		this.x = newX;

		System.out.println("\tthis.x = " + this.x);
	}

	// Method
	//      public                  : is een method die overal kan gebruikt kan worden.
	//      int                     : return type, in dit geval een int.
	//      methodWithReturnValue   : naam van de method.
	//      ()                      : geeft aan dat het een method is.
	//      {}                      : block die de statements bevat die worden uitgevoerd in de method.
	//      return result           : op het einde van de method return je het resultaat.
	//
	// name + parametertypes are called method signature
	// Called in the Main class, op het object rectangle.

	public int methodWithReturnValue() {
		int result = 0;

		System.out.println("In methodWithReturnValue van Rectangle.");

		result = 100 + x;       // this is not needed if there is no conflict with a local variable.

		return result;          // return the velue of result to the caller (in this case Main).
	}

	// Method overloading : important Object Oriented principle.
	// 2 Methods with the same name, but DIFFERENT PARAMETER TYPES i.e.
	// a different signature.
	//
	public int sum(int first, int second) {
		return first + second;
	}

	public double sum(double first, double second) {
		return first + second;
	}

	// The toString method returns the state of the object as text.
	// It is recommended to add this method (ALT+INSERT),
	// can be handy when developing to trace values of the attributes of an object
	//
	public String toString() {
		String result = "";
		result += "Rectangle that is " + height;
		result += " high and " + width;
		result += " wide, with left top at point (" + x + ", " + y + ").";

		return result;
	}
}
