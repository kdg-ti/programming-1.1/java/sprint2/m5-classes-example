public class Main {
	// OO programming continued:
	//
	// This class contains the main method and is used to run the program
	// You can find the definition of the Rectangle class in graphics/Rectangle.java.
	//
	// We start by making a Rectangle object, called objectOfClassRectangle.
	// Once the object exists we can use it by calling methods on it.
	//
	// The steps in this program are in /* block comments */
	// You can toggle block comments by selecting the text and main menu "code|comment with block comment" of CTRL+SHIFT+/

	public static void main(String[] args) {
        /*
        // Declaration and construction of an object of class Rectangle, called objectOfClassRectangle.
        // The keyword 'new' calls the constructor.
        // In this case this is the constructor without parameters.
        // Making an object is also called instantiating, or making an instance of the class.

        Rectangle objectOfClassRectangle = new Rectangle();

        System.out.println("height van objectOfClassRectangle = " + objectOfClassRectangle.getHeight());

        // 2 more objects of class Rectangle.
        Rectangle rectangle_1 = new Rectangle(50, 150);
        Rectangle rectangle_2 = new Rectangle(25, 200);

        System.out.println("height van rectangle_1 = " + rectangle_1.getHeight());
        System.out.println("height van rectangle_2 = " + rectangle_2.getHeight());

        // Yet another object, made using the copy constructor.
        Rectangle rectangle_3 = new Rectangle(rectangle_1);

        System.out.println("height van rectangle_1 = " + rectangle_1.getHeight());
        System.out.println("height van rectangle_3 = " + rectangle_3.getHeight());
        */

        /*
        // Call  methods on the object.
        Rectangle rectangle = new Rectangle();

        rectangle.methodNoArguments();
        rectangle.methodWithArgument(15);
        int valueFromMethod = rectangle.methodWithReturnValue();

        System.out.println("\tValue received from object rectangle = " + valueFromMethod);

        rectangle.setHeight(500);
        System.out.println("My new height = " + rectangle.getHeight());
        */

        /*
        // Method overloading
        Rectangle rectangle = new Rectangle();

        System.out.println("Sum of integers = " + rectangle.sum(5, 8));
        System.out.println("Sum of doubles = " + rectangle.sum(5.5, 8.5));
        */

        /*
        // Call  toString method, on the object:
        Rectangle rectangle = new Rectangle(100, 200);
        System.out.println(rectangle.toString());
        */
	}
}
